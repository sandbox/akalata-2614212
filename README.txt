INTRODUCTION
------------

The Link Text module provides an additional formatter for the core Link field
type in order to define a consistent link title using Manage Display.

Options include:

 * Use the field label as the link text
 * Provide a custom string to use as the link text (similar to the "Static
   Title" option in the Link module.
 * Wrap the link text in additional inline tags, including <span>, <strong>,
   or <em>


REQUIREMENTS
------------

This module has no special requirements.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-8
   for further information.


CONFIGURATION
-------------

 * You may configure link fields to use this formatter under Administration »
   Content Types » [your content type] » Manage Display.
