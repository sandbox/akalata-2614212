<?php

/**
 * @file
 * Contains \Drupal\link_text\Plugin\Field\FieldFormatter\LinkTextFormatter.
 */

namespace Drupal\link_text\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\link\Plugin\Field\FieldFormatter\LinkFormatter;
use Drupal\Core\Form\FormStateInterface;
use \Drupal\Core\Url;
use \Drupal\Component\Utility\SafeMarkup;

/**
 * Plugin implementation of the 'link_text' formatter.
 *
 * @FieldFormatter(
 *   id = "link_text",
 *   label = @Translation("Define link text"),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class LinkTextFormatter extends LinkFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return array(
      'use_label' => FALSE,
      'text' => '',
      'wrapper' => '',
      'rel' => '',
      'target' => '',
    ) + parent::defaultSettings();;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    unset($elements['trim_length']);
    unset($elements['url_only']);
    unset($elements['url_plain']);

    $elements['use_label'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use field label'),
      '#default_value' => $this->getSetting('use_label'),
      '#description' => t('Use the field label as the link text.'),
    );

    $elements['text'] = array(
      '#type' => 'textfield',
      '#title' => t('Link text'),
      '#default_value' => $this->getSetting('text'),
      '#description' => t('Provide custom link text. This will override the "Use field label" option above.'),
    );

    $elements['wrapper'] = array(
      '#type' => 'select',
      '#title' => t('Link text wrapper'),
      '#default_value' => $this->getSetting('wrapper'),
      '#description' => t('Define a wrapper for the linked text. This HTML tag will appear inside the link element.'),
      '#options' => $this->getElements(),
    );

    $reorder = array();
    foreach (array('use_label', 'text', 'wrapper', 'rel', 'target') as $item) {
      $reorder[$item] = $elements[$item];
    }

    return $reorder;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = array();

    $settings = $this->getSettings();

    if (!empty($settings['text'])) {
      $summary[] = t('Link text: @text', array('@text' => $settings['text']));
    }
    elseif ($settings['use_label']) {
      $summary[] = t('Link text will use field label');
    }
    if (!empty($settings['wrapper'])) {
      $summary[] = t('Link text will be wrapped in @tag tags', array('@tag' => $settings['wrapper']));
    }
    if (!empty($settings['rel'])) {
      $summary[] = t('Add rel="@rel"', array('@rel' => $settings['rel']));
    }
    if (!empty($settings['target'])) {
      $summary[] = t('Open link in new window');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = array();
    $settings = $this->getSettings();

    foreach ($items as $delta => $item) {
      // By default use the full URL as the link text.
      $url = $this->buildUrl($item);
      $link_text = $url->toString();

      if (!empty($settings['text'])) {
        // Unsanitized settings text here because the entire link title gets
        // auto-escaped during link generation in
        // \Drupal\Core\Utility\LinkGenerator::generate().
        $link_text = $settings['text'];
      }
      elseif ($settings['use_label']) {
        // Get label from field.
        $field = $item->getFieldDefinition();
        $link_text = $field->getLabel();
      }

      if (!empty($settings['wrapper'])) {
        $wrapped_title = SafeMarkup::format('<@wrapper>@text</@wrapper>', array('@text' => $link_text, '@wrapper' => $settings['wrapper']));

        $element[$delta] = array(
          '#markup' => \Drupal::l($wrapped_title, $url),
        );

      }
      else {
        $element[$delta] = array(
          '#type' => 'link',
          '#title' => $link_text,
          '#options' => $url->getOptions(),
          '#url' => $url,
        );
      }

      if (!empty($item->_attributes)) {
        $element[$delta]['#options'] += array('attributes' => array());
        $element[$delta]['#options']['attributes'] += $item->_attributes;
        // Unset field item attributes since they have been included in the
        // formatter output and should not be rendered in the field template.
        unset($item->_attributes);
      }
    }
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function getElements() {
    static $elements = NULL;
    if (!isset($elements)) {
      $elements = array(
        '' => $this->t(' - Use default -'),
        '0' => $this->t('- None -')
      );
      $elements += \Drupal::config('link_text.settings')->get('inline_elements');
    }

    return $elements;
  }

}
