<?php

/**
 * @file
 * Contains \Drupal\link_text\Tests\LinkTextTest.
 */

namespace Drupal\link_text\Tests;

use Drupal\simpletest\WebTestBase;

/**
 * Tests the link_text field formatter.
 *
 * @group link_text
 */
class LinkTextTest extends WebTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = array('link_text, link');

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    // Add a link field to the Basic Page node.

    // Create user then login.
    // @TODO: User must be able to view and administer content
    $this->user = $this->drupalCreateUser(array(
      'administer site configuration',
      'access sitemap',
    ));
    $this->drupalLogin($this->user);
  }

  /**
   * Tests link_text field formatter options
   */
  public function testLinkText() {
    // Tests text replacement with field label.

    // Tests text replacement with a custom string.

    // Tests wrapping the link text in configured tags.
    // @TODO: Settings loop around the settings options
  }

}
